(function () {

    angular.module('Fifa.Matchday').controller('NewsController', [ 'NewsModel', '$stateParams', '$rootScope', function (NewsModel, $stateParams, $rootScope) {

        var news = this;
        var newsId = $stateParams.newsId;

        news.newsId = newsId;
        news.nextNewsId = Math.max(1, (parseInt(newsId) + 1) % 5);

        NewsModel.getById(newsId)
            .then(function(result){
                news.currentNews = (result !== null) ? result : [{content: 'No news found'}];
                $rootScope.newsDisplayed = true;
            });

    }]);

})();

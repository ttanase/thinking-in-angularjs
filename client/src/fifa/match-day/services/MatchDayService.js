(function() {

    angular.module('Fifa.Matchday').service('MatchDayService', function(NewsModel) {

        var service = this;

        service.getNews = NewsModel.all;
    });
})();
(function () {
    var fifaModule = angular.module('Fifa', [
        'ngRoute',
        'ui.router',
        'ngMessages',
        'Fifa.Common',
        'Fifa.Dashboard',
        'Fifa.Matchday',
        'Fifa.Play'
    ]);

    fifaModule.factory('loadingInterceptor', function (LoadingService) {
        var loadingInterceptor = {
            request: function (config) {
                LoadingService.setLoading(true);
                return config;
            },
            response: function (response) {
                LoadingService.setLoading(false);
                return response;
            }
        };
        return loadingInterceptor;
    });


    fifaModule.config(['$stateProvider', '$httpProvider', '$locationProvider', '$sceDelegateProvider',
        function ($stateProvider, $httpProvider, $locationProvider, $sceDelegateProvider) {

            $stateProvider
                .state('dashboard', {
                    url: '/',
                    templateUrl: 'src/fifa/dashboard/views/dashboard.html',
                    controller: '',
                    controllerAs: ''
                })
                .state('play', {
                    url: '/play',
                    templateUrl: 'src/fifa/play/views/play.html',
                    controller: 'PlayController',
                    controllerAs: 'play'
                })

                .state('match-day', {
                    url: '/match-day',
                    templateUrl: 'src/fifa/match-day/views/match-day.html',
                    controller: 'MatchDayController',
                    controllerAs: 'matchDay'
                })

                .state('match-day.news', {
                    url: '/news/:newsId',
                    templateUrl: 'src/fifa/match-day/views/match-day-news.html',
                    controller: 'NewsController',
                    controllerAs: 'news'
                })
            ;


            $httpProvider.interceptors.push('loadingInterceptor');

            $sceDelegateProvider.resourceUrlWhitelist(['**']);

        }


    ]);


})();




    angular.module('Fifa.Dashboard').controller( 'LatestMatchController', ['$scope', 'ScoresService',  function($scope, ScoresService){


        var latestScore = ScoresService.getLatestMathScore();

        $scope.homeTeam = latestScore.homeTeam;

        $scope.awayTeam = latestScore.awayTeam;
    }]);


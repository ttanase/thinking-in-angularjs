angular.module('Fifa.Dashboard').directive('myPopup', function(){

    return {
        restrict: 'EA',
        scope: true,
        controller: function() {},
        transclude: true,
        replace: true,
        templateUrl: 'src/fifa/dashboard/views/my-popup.html',
        link: function(scope, element, attr) {
            $(element).modal('toggle');
        }

    };
});
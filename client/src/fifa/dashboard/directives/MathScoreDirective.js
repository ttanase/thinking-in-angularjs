angular.module('Fifa.Dashboard').directive('matchDirective', function(){

    return {
        restrict: 'EA',
        scope: true,
        controller: function() {},
        templateUrl: 'src/fifa/dashboard/views/match-directive.html'
    };
});
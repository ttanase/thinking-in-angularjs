(function() {

    angular.module('Fifa.Dashboard').service('ScoresService', function() {

        var service = this;



        service.getLatestMathScore = function() {

            return {
                homeTeam: {
                    name: "Bayern",
                    score: 1
                },
                awayTeam: {
                    name: "Dortmund",
                    score: 3
                }
            }
        };
    });
})();
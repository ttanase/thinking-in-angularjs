angular.module('Fifa.Common')
    .service('NewsModel',
        function ($http, EndpointConfigService, UtilsService) {
            var service = this,
                MODEL = 'news';

            service.all = function () {
                return $http.get(EndpointConfigService.getUrl(MODEL))
                    .then(
                        function(result) {
                            return UtilsService.objectToArray(result);
                        }
                    );
            };

            service.getById = function (newsId) {
                return $http.get(
                    EndpointConfigService.getUrlForId(MODEL, newsId)
                ).then(
                    function(result) {
                        return UtilsService.objectToArray(result);
                    }
                );
            };

            service.create = function (news) {
                return $http.post(
                    EndpointConfigService.getUrl(MODEL),
                    news
                );
            };

            service.update = function (newsId, news) {
                return $http.put(
                    EndpointConfigService.getUrlForId(MODEL, newsId), news
                );
            };

            service.delete = function (newsId) {
                return $http.delete(
                    EndpointConfigService.getUrlForId(MODEL, newsId)
                );
            };
        });
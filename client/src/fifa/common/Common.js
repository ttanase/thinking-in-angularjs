(function () {

    var commonModule = angular.module('Fifa.Common', [
    ]);

    commonModule.config(function(locationBasedNewsProvider){

        locationBasedNewsProvider.setCurrentLocation("");
    });

})();
angular.module('Fifa.Common')

    .constant('REST_API_HOST', 'http://localhost')
    .constant('REST_API_PORT', '3001')


    .service('EndpointConfigService', function(REST_API_HOST, REST_API_PORT) {
        var service = this;

        service.getUrl = function(model) {
            return REST_API_HOST + ":" + REST_API_PORT + "/" + model;
        };

        service.getUrlForId = function(model, id) {
            return service.getUrl(model) + "/" + id;
        };
    });

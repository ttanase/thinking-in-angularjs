angular.module('Fifa.Common')
    .service('UtilsService',
        function() {
            var service = this;

            service.objectToArray = function(content) {

                return content.data;

                // normalizes data so it gets returned as arrays
                if (content.data instanceof Object && !Array.isArray(content.data)) {
                    var newArray = [];

                    for (var key in content.data) {
                        var item = content.data[key];
                        item.id = key;
                        newArray.push(item);
                    }

                    return newArray;
                
                } else {
                    return content.data;
                }
            };
        }
    );
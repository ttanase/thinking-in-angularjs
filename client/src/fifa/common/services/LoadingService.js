angular.module('Fifa.Common')
    .service('LoadingService',
        function ($rootScope) {
            var service = this;

            $rootScope.loadingView = false;

            service.setLoading = function(loading) {
                $rootScope.loadingView = loading;
            };
        });

(function () {

    angular.module('Fifa.Common').provider('locationBasedNews', function () {

        var provider = this;

        provider.currentLocation = "";

        provider.setCurrentLocation = function (newLocation) {
            provider.currentLocation = newLocation;
        };

        this.$get = function ($http, EndpointConfigService, UtilsService) {
            return {
                all: function () {
                    return $http.get(EndpointConfigService.getUrl('news'))
                        .then(
                            function (result) {
                                var formattedResult = UtilsService.objectToArray(result);

                                if (provider.currentLocation.length) {
                                    var filteredData = [];

                                    for (var i = 0; i < formattedResult.length; i++) {
                                        if (formattedResult[i].tags.indexOf(provider.currentLocation) >= 0) {
                                            filteredData.push(formattedResult[i]);
                                        }
                                    }

                                    return filteredData;

                                } else {
                                    return formattedResult;
                                }
                            }
                        );
                }
            };
        };
    });
})();


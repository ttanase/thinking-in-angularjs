(function () {

    angular.module('Fifa.Play').controller('PlayController', function () {

        var play = this;

        play.mediaContent = [
            {
                "type": "image",
                "title": "Zlatan's wisdom",
                "src": "https://s-media-cache-ak0.pinimg.com/736x/f4/20/93/f42093a8a1bd60db64d1859acb9a6066.jpg",
                "description": ""
            },
            {"type": "video",
                "title": "Zlatan's best goal",
                "src": "https://www.youtube.com/embed/GcCVfNA7otY"
            },
            // {
            //     "type": "text",
            //     "title": "Hunter S. Thompson, Hell's Angels: A Strange and Terrible Saga",
            //     "src": "The Edge... There is no honest way to explain it because the only people who really know where it is are the ones who have gone over."
            // },
        ];
    });

})();

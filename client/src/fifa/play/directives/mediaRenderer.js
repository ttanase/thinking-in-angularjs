(function () {


    angular.module('Fifa.Play').directive('mediaRenderer', function ($compile) {

        var imageTpl = '<div class="media"><div class="media-left"><h2 class="media-heading">{{content.title}}</h2><a href="{{content.src}}"><img class=""  width="440" ng-src="{{content.src}}" alt="content.title"></a></div><div class="media-body"><p>{{content.description}}</p></div></div>';
        var videoTpl = '<div class="entry-video"><h2>{{content.title}}</h2><div class="entry-vid"><iframe src="{{content.src}}" width="100%" height="300" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div><div class="entry-text"><div class="text-justify">{{content.description}}</div></div></div>';
        var textTpl = '<div class="panel panel-default"><div class="panel-body"><h1>{{content.title}}</h1><p class="lead">{{content.src}}</p></div></div>';

        var getTemplate = function(type) {
            var template = '';

            switch(type) {
                case 'image':
                    template = imageTpl;
                    break;
                case 'video':
                    template = videoTpl;
                    break;
                case 'text':
                    template = textTpl;
                    break;
            }

            return template;
        };

        var link = function(scope, element, attrs) {

            var template = getTemplate(scope.content.type);

            var linkFn = $compile(template);
            var content = linkFn(scope);
            element.append(content);

        };

        return {
            restrict: "E",
            link: link,
            scope: {
                content:'='
            }
        };
    })
})();
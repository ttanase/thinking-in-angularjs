head.load(
    {file: 'node_modules/angular/angular.js'},
    {file: 'node_modules/angular-route/angular-route.js'},
    {file: 'node_modules/angular-messages/angular-messages.js'},
    {file: 'node_modules/angular-ui-router/release/angular-ui-router.js'},
    {file: 'node_modules/jquery/dist/jquery.js'},
    {file: 'node_modules/chart.js/dist/Chart.min.js'},
    {file: 'node_modules/bootstrap/dist/js/bootstrap.js'},
    {file: 'node_modules/bootstrap/dist/css/bootstrap.css'},



    {file: "src/fifa/Fifa.js"},

    {file: "src/fifa/common/Common.js"},
    {file: "src/fifa/common/controllers/MainController.js"},
    {file: "src/fifa/common/models/NewsModel.js"},
    {file: "src/fifa/common/services/EndpointConfigService.js"},
    {file: "src/fifa/common/services/UtilsService.js"},
    {file: "src/fifa/common/services/LoadingService.js"},


    {file: "src/fifa/common/providers/LocationBasedNewsProvider.js"},

    {file: "src/fifa/dashboard/Dashboard.js"},
    {file: "src/fifa/dashboard/services/ScoresService.js"},
    {file: "src/fifa/dashboard/controllers/LatestMatchController.js"},
    {file: "src/fifa/dashboard/directives/MathScoreDirective.js"},
    {file: "src/fifa/dashboard/directives/MyPopup.js"},


    {file: "src/fifa/match-day/Matchday.js"},
    {file: "src/fifa/match-day/services/MatchDayService.js"},
    {file: "src/fifa/match-day/controllers/MatchDayController.js"},
    {file: "src/fifa/match-day/controllers/NewsController.js"},
    {file: "src/fifa/match-day/controllers/ClickerController.js"},


    {file: "src/fifa/play/Play.js"},
    {file: "src/fifa/play/controllers/PlayController.js"},
    {file: "src/fifa/play/directives/mediaRenderer.js"}
);

